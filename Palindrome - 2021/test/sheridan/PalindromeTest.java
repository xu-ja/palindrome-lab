package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		assertTrue( "String is not a palindrome...", Palindrome.isPalindrome("dad") );
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse( "String is a palindrome", Palindrome.isPalindrome("string") );
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue( "Unable to validate palindrome...", Palindrome.isPalindrome("edit tide") );
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse( "Unable to validate palindrome...", Palindrome.isPalindrome("edit on tide") );
	}	
	
}
